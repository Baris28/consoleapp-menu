﻿using System;

namespace ConsoleApp
{
    public class Calculator
    {
        private double _num1;
        private double _num2;
        private double _result;

        public void AddSum()
        {
            Console.Clear();
            // Converts the inserted value to a double
            Console.WriteLine("Type the first number here:");
            _num1 = Convert.ToDouble(Console.ReadLine());
            // Converts the inserted value to a double
            Console.WriteLine("\nType the second number here:");
            _num2 = Convert.ToDouble(Console.ReadLine());

            // Put in the result variable the value inserted in num1 and num2;
            _result = _num1 + _num2;

            // Write it to the user.
            Console.WriteLine($"\nResult is: {_num1} + {_num2} = {_result}\n");
        }

        public void SubtractSum()
        {
            Console.Clear();

            Console.WriteLine("Type the first number here:");
            _num1 = Convert.ToDouble(Console.ReadLine());

            Console.WriteLine("\nType the second number here:");
            _num2 = Convert.ToDouble(Console.ReadLine());

            // Put in the result variable the value inserted in num1 and num2;
            _result = _num1 - _num2;

            // Write it to the user.
            Console.WriteLine($"\nResult is: {_num1} - {_num2} = {_result}\n");
        }

        public void MultiplySum()
        {
            Console.Clear();

            Console.WriteLine("Type the first number here:");
            _num1 = Convert.ToDouble(Console.ReadLine());

            Console.WriteLine("\nType the second number here:");
            _num2 = Convert.ToDouble(Console.ReadLine());

            // Put in the result variable the value inserted in num1 and num2;
            _result = _num1 * _num2;

            // Write it to the user.
            Console.WriteLine($"\nResult is: {_num1} * {_num2} = {_result}\n");
        }

        public void DivideSum()
        {
            Console.Clear();

            Console.WriteLine("Type the first number here:");
            _num1 = Convert.ToDouble(Console.ReadLine());

            Console.WriteLine("\nType the second number here:");
            _num2 = Convert.ToDouble(Console.ReadLine());

            // Put in the result variable the value inserted in num1 and num2;
            _result = _num1 / _num2;

            // Write it to the user.
            Console.WriteLine($"\nResult is: {_num1} / {_num2} = {_result}\n");
        }
    }
}
