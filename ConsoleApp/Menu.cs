﻿using System;

namespace ConsoleApp
{
    public class Menu
    {
        private string _insertedName;

        public void MenuOption()
        {

            // C# Menu
            Console.WriteLine("To navigate the menu choose a number out of this list, and then press enter...\n");
            Console.WriteLine("------------------------------------------------------------------------------\n");
            Console.WriteLine("1. Insert name here.\n2. Insert two numbers that you want to calculate here.");
            Console.WriteLine("3. Output your name.\n4. Exit Console.");

            Console.Write("\nSelect a option: ");
            var menuChoice = Console.ReadLine();

            switch (menuChoice)
            {
                case "1":
                    // Call the ReadOutName method
                    ReadOutName();
                    // Exit if any key other then 1 is pressed
                    ExitOption();
                    break;

                case "2":
                    // Call the AddSum method from Calculator class
                    CalcMenu();
                    ExitOption();
                    break;

                case "3":
                    // Clear console
                    Console.Clear();
                    // Checks if the variable is empty
                    if(string.IsNullOrEmpty(_insertedName)) { Console.WriteLine("Name isn't set yet\n"); ExitOption();}
                    // Show the inserted name result
                    Console.WriteLine($"Your name is: {_insertedName}\n");
                    ExitOption();
                    break;
                case "4":
                    // Exit if any other key is pressed at start menu
                    Environment.Exit(0);
                    break;
                default:
                    Console.Clear();
                    MenuOption();
                    break;
            }
        }


        private void ReadOutName()
        {
            // Clear Console
            Console.Clear();

            Console.WriteLine("Type your name here:");

            // Converts the inserted value to a string
            _insertedName = Convert.ToString(Console.ReadLine());

            Console.WriteLine($"Your name is: {_insertedName}\n");
        }

        private void ExitOption()
        {
            Console.WriteLine("Press a key to go back to the menu");
            // Check key press then clear console and go back to menuOption
            Console.ReadKey();
            Console.Clear();
            MenuOption();
        }


        private void CalcMenu()
        {
            Console.Clear();
            // Call class
            var calc = new Calculator();

            Console.WriteLine("1. Add\n2. Minus\n3. Multiply\n4. Divide");
            Console.WriteLine("\nSelect one of the option above to calculate it this way: ");

            var calcMenuChoice = Console.ReadLine();

            Console.WriteLine("1. Add 2 numbers up");
            Console.WriteLine("2. Minus 2 numbers up");
            Console.WriteLine("3. Multiply 2 numbers up");
            Console.WriteLine("4. Divide 2 numbers up");
            switch (calcMenuChoice)
            {
                case "1":
                    // Calculates the 2 numbers and adds them together
                    calc.AddSum();
                    ExitOption();
                    break;
                case "2":
                    // Calculates the 2 numbers and subtracts
                    calc.SubtractSum();
                    ExitOption();
                    break;
                case "3":
                    // Calculates the 2 numbers and multiplies
                    calc.MultiplySum();
                    ExitOption();
                    break;

                case "4":
                    // Calculates the 2 numbers and divides
                    calc.DivideSum();
                    ExitOption();
                    break;
                default:
                    // Clear console and goes back to calcMenu if one of the above numbers isn't selected
                    Console.Clear();
                    CalcMenu();
                    break;
            }
        }
    }
}