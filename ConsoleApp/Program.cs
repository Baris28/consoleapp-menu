﻿namespace ConsoleApp
{
    internal static class Program
    {
        private static void Main()
        {
            // Call class
            var menuChoices = new Menu();

            // Call MenuOption method from menuChoices class
            menuChoices.MenuOption();
        }
    }
}